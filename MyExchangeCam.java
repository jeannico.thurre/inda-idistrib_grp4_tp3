// CODE FROM MATHIEU CAMELIQUE GROUP
import mpi.*;

public class MyExchangeCam {

    public final static int MAX_LENGTH = 1010;

    public static void main(String[] args) throws MPIException {
        MPI.Init(args);

        int myrank = MPI.COMM_WORLD.getRank();

        int[] basicMessageSent = new int[MAX_LENGTH];
        for (int i = 0; i < basicMessageSent.length; i++) {
            basicMessageSent[i] = i;
        }

        int[] basicMessageReceived = new int[MAX_LENGTH];

        System.out.println("Test with message of length " + basicMessageSent.length);

        if (myrank == 0) {
            int firstMessageSentTag = 0;

            MPI.COMM_WORLD.send(basicMessageSent, basicMessageSent.length, MPI.INT, 1, firstMessageSentTag);
            System.out.println("Message " + firstMessageSentTag + " sent by MPI rank " + myrank);

            int secondMessageSentTag = 1;
            MPI.COMM_WORLD.send(basicMessageSent, basicMessageSent.length, MPI.INT, 1, secondMessageSentTag);
            System.out.println("Message " + secondMessageSentTag + " sent by MPI rank " + myrank);

            int firstMessageReceivedTag = 4;
            MPI.COMM_WORLD.recv(basicMessageReceived, basicMessageReceived.length, MPI.INT, 1, firstMessageReceivedTag);
            System.out.println("Message " + firstMessageReceivedTag + " received by MPI rank " + myrank + " : " + basicMessageReceived);
        } else if (myrank == 1) {
            int firstMessageReceivedTag = 1;
            MPI.COMM_WORLD.recv(basicMessageReceived, basicMessageReceived.length, MPI.INT, 0, firstMessageReceivedTag);
            System.out.println("Message " + firstMessageReceivedTag + " received by MPI rank " + myrank + " : " + basicMessageReceived);
int firstMessageSentTag = 2;
            MPI.COMM_WORLD.send(basicMessageSent, basicMessageSent.length, MPI.INT, 2, firstMessageSentTag);
            System.out.println("Message " + firstMessageSentTag + " sent by MPI rank " + myrank);

            int secondMessageReceivedTag = 3;
            MPI.COMM_WORLD.recv(basicMessageReceived, basicMessageReceived.length, MPI.INT, 2, secondMessageReceivedTag);
            System.out.println("Message " + secondMessageReceivedTag + " received by MPI rank " + myrank + " : " + basicMessageReceived);

            int thirdMessageReceivedTag = 0;
            MPI.COMM_WORLD.recv(basicMessageReceived, basicMessageReceived.length, MPI.INT, 0, thirdMessageReceivedTag);
            System.out.println("Message " + thirdMessageReceivedTag + " received by MPI rank " + myrank + " : " + basicMessageReceived);

            int secondMessageSentTag = 4;
            MPI.COMM_WORLD.send(basicMessageSent, basicMessageSent.length, MPI.INT, 0, secondMessageSentTag);
            System.out.println("Message " + secondMessageSentTag + " sent by MPI rank " + myrank);
        } else if (myrank == 2) {
            int firstMessageReceivedTag = 2;
            MPI.COMM_WORLD.recv(basicMessageReceived, basicMessageReceived.length, MPI.INT, 1, firstMessageReceivedTag);
            System.out.println("Message " + firstMessageReceivedTag + " received by MPI rank " + myrank + " : " + basicMessageReceived);

            int firstMessageSentTag = 3;
            MPI.COMM_WORLD.send(basicMessageSent, basicMessageSent.length, MPI.INT, 1, firstMessageSentTag);
            System.out.println("Message " + firstMessageSentTag + " sent by MPI rank " + myrank);
        }

        MPI.Finalize();
    }
}