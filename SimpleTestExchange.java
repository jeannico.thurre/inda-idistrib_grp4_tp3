import mpi.*;
import java.io.*;
import java.util.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SimpleTestExchange {
    static int myrank;

    public static void send(int messageTag, int[] message, int destination) throws MPIException  {
        MPI.COMM_WORLD.send(message, message.length, MPI.INT, destination, messageTag);
        printWithTimestamp("[" + myrank + "] " + "Sent " + messageTag);
    }

    public static int[] receive(int messageTag, int messageSize) throws MPIException  {
        int[] received = new int[messageSize];
        MPI.COMM_WORLD.recv(received, messageSize, MPI.INT, MPI.ANY_SOURCE, messageTag);
        printWithTimestamp("[" + myrank + "] " + "Received " + messageTag + " : " + java.util.Arrays.toString(received));
        
        return received;
    }

    private static void printWithTimestamp(String message) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timestamp = dateFormat.format(new Date());
        System.out.println(timestamp + " " + message);
    }

    public static void main(String[] args) throws MPIException {
        // START
        MPI.Init(args);

        // IDENTITY
        myrank = MPI.COMM_WORLD.getRank();

        //IDENTITY
        myrank = MPI.COMM_WORLD.getRank();

        //MESSAGE GENERATION
        final int MAX_LENGTH = 1010;
        int[] basicMessage = new int[MAX_LENGTH];
        for (int i = 0; i < basicMessage.length; i++) {
            basicMessage[i] = i;
        }

        if (myrank == 0) {
            // Envoie Tag 3 à Rank 1
            send(3, basicMessage, 1);
            
            // Envoie Tag 1 à Rank 1
            send(1, basicMessage, 1);
            
            // Attend Réception Tag 2
            receive(2, basicMessage.length);
        } else if (myrank == 1) {
            // Attend Réception Tag 1 du Rank 1
            receive(1, basicMessage.length);

            // Envoie Tag 2 à Rank 0
            send(2, basicMessage, 0);

            // Attend Réception Tag 3 du Rank 1
            receive(3,basicMessage.length);

            System.out.println("END");
        }

        // END
        MPI.Finalize(); 
    }
}