package _git;
import mpi.*;

public class MyExchange {
    public static void main(String[] args) throws MPIException {
        //START
        MPI.Init(args);

        //IDENTITY
        int myrank = MPI.COMM_WORLD.getRank();
        int messageTag = 10;

        if (myrank == 0) {

            //SENDING message
            int message[] = new int[] { 1, 2, 3, 4, 5, 6 };
            MPI.COMM_WORLD.send(message, message.length, MPI.INT, 1, messageTag);
            System.out.println("Rank 0 has sent ");

        } else if (myrank == 1) {

            //RECEVING message
            int[] received = new int[6];
            MPI.COMM_WORLD.recv(received, 6, MPI.INT, 0, messageTag);
            System.out.println("Rank 1 received ! " + java.uti.Arrays.toString(received));

        }

        //END
        MPI.Finalize(); 
    }
}