# Compile
mpijavac MyExchange.java

# Run
mpirun java MyExchange

# Git
cd ~
git clone https://gitlab.forge.hefr.ch/jeannico.thurre/inda-idistrib_grp4_tp3.git
cd ~/inda-idistrib_grp4_tp3

## Combined git push
git add * ; git commit -m "Testing files" ; git push origin main

# Combined
## MyExchange
git pull && mpijavac MyExchange.java && mpirun java MyExchange

## Simple
git pull && mpijavac SimpleTestExchange.java && mpirun java SimpleTestExchange

## Mathieu Camélique's group code
git pull && mpijavac MyExchangeCam.java && mpirun java MyExchangeCam
