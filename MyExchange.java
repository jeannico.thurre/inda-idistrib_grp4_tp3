import mpi.*;
import java.io.*;
import java.util.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyExchange {
    static int myrank;

    public static void send(int messageTag, int[] message, int destination) throws MPIException  {
        MPI.COMM_WORLD.send(message, message.length, MPI.INT, destination, messageTag);
        printWithTimestamp("[" + myrank + "] " + "Sent " + messageTag);
    }

    public static int[] receive(int messageTag, int messageSize) throws MPIException  {
        int[] received = new int[messageSize];
        printWithTimestamp("[" + myrank + "] " + "Waiting " + messageTag);
        MPI.COMM_WORLD.recv(received, messageSize, MPI.INT, MPI.ANY_SOURCE, messageTag);
        printWithTimestamp("[" + myrank + "] " + "Received " + messageTag + " : " + received.length + " bits long");
        
        return received;
    }

    private static void printWithTimestamp(String message) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timestamp = dateFormat.format(new Date());
        System.out.println(timestamp + " " + message);
    }

    public static void main(String[] args) throws MPIException {
        //START
        MPI.Init(args);

        //IDENTITY
        myrank = MPI.COMM_WORLD.getRank();

        //MESSAGE GENERATION
        // Get size from argument passed in command or 1010 by default
        //     mpirun java MyExchange 1010
        int messageSize = 1010;
        if (args.length > 0) {
            messageSize = Integer.parseInt(args[0]);
        }

        int[] basicMessage = new int[messageSize];

        //PROCESSING MESSAGES
        if (myrank == 0) {
            //SEND Tag 0 to Rank 1
            send(0, basicMessage, 1);
            
            //SEND Tag 1 to Rank 1
            send(1, basicMessage, 1);

            //WAITING Tag 4
            receive(4, basicMessage.length);
            
        } else if (myrank == 1) {
            //WAITING Tag 1
            receive(1, basicMessage.length);
            
            //SENDING Tag 2 to Rank 2
            send(2, basicMessage, 2);
            
            //WAITING Tag 3
            receive(3, basicMessage.length);
            
            //WAITING Tag 0
            receive(0, basicMessage.length);

            //SENDING Tag 4 to Rank 0
            send(4, basicMessage, 0);

        } else if (myrank == 2) {
            //WAITING Tag 2
            receive(2, basicMessage.length);
            
            //SENDING Tag 3 to Rank 1
            send(3, basicMessage, 1);
        }

        //END
        MPI.Finalize(); 
    }
}